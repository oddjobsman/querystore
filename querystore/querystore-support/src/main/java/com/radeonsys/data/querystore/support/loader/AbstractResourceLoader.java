/*
 * @(#)AbstractResourceLoader.java				Thu May 31, 2012 03:39 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore.support.loader;

import com.google.common.io.InputSupplier;

import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 * An abstract implementation of {@see ResourceLoader} which can be extended by concrete implementation.
 * 
 * <p>Concrete implementations only have to implement the {@see #getReadableResource(String, Charset)} 
 * method.</p> 
 *
 * <p>Provides a convenient default implementation of the {@see ResourceLoader#getBinaryResource(String))} 
 * method which throws an {@see UnsupportedOperationException}.</p>
 *
 * @author oddjobsman
 */
public abstract class AbstractResourceLoader implements ResourceLoader {

	/**
     * Returns a factory that provides a {@see Reader} over the resource at the specified location.
     *
     * <p>This method uses the default encoding set for this Java environment or the underlying 
     * operating system. Calling this method is equivalent to calling:
     * 
     * <code>
     * loader.getReadableResource("resourceLocation", Charset.defaultCharset());
     * </code>
     * </p>
     *
     * @param resourceLocation  the location of the resource that is required
     *
     * @return  reference to an instance of {@see InputSupplier} which supplies
     *          the content of the resource as a character stream
     *
     * @throws  ResourceNotFoundException
     *          if no resource could be found at the specified location
     */
    @Override
    public InputSupplier<? extends Reader> getReadableResource(String resourceLocation) {
    	return getReadableResource(resourceLocation, Charset.defaultCharset());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public abstract InputSupplier<? extends Reader> getReadableResource(String resourceLocation, Charset charset);

    /**
     * Provides a default implementation of the {@see ResourceLoader#getBinaryResource} method by throwing
     * an {@see UnsupportedOperationException}
     *
     * {@inheritDoc}
     */
    @Override
    public InputSupplier<? extends InputStream> getBinaryResource(String resourceLocation) {
        throw new UnsupportedOperationException("This operation is not supported by this implementation");
    }

}
