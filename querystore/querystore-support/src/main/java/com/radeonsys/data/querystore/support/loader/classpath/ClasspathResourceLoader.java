/*
 * @(#)ClasspathResourceLoader.java				Jun 1, 2012 11:45:00 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.radeonsys.data.querystore.support.loader.classpath;

import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.io.InputSupplier;
import com.google.common.io.Resources;
import com.radeonsys.data.querystore.support.loader.AbstractResourceLoader;
import com.radeonsys.data.querystore.support.loader.ResourceNotFoundException;

/**
 * @author oddjobsman
 */
public class ClasspathResourceLoader extends AbstractResourceLoader {

	/** 
	 * Returns a factory that provides a {@see Reader} implementation over the resource at the
     * specified location on the classpath using the specified character set to decode
     * the contents of the resource
     *
     * @param resourceLocation	the location of the resource on the classpath
     * @param charset           reference to an instance of {@see Charset} that represents the
     *                          character set used to decode the contents of the resource
     *
     * @return  reference to an instance of {@see InputSupplier} which provides instances of
     *          {@see Reader} over the resource at the specified location on the classpath
	 */
	@Override
	public InputSupplier<? extends Reader> getReadableResource(String resourceLocation, Charset charset) {
		return Resources.newReaderSupplier(getResourceUrl(resourceLocation), charset);
	}
	
	/** 
	 * Returns a factory that provides a {@see InputStream} implementation over the resource at the
     * specified location on the classpath
     *
     * @param resourceLocation	the location of the resource on the classpath
     *
     * @return  reference to an instance of {@see InputSupplier} which provides instances of
     *          {@see InputStream} over the resource at the specified location on the classpath
	 */
	@Override
	public InputSupplier<? extends InputStream> getBinaryResource(String resourceLocation) {
		return Resources.newInputStreamSupplier(getResourceUrl(resourceLocation));
	}

	/**
	 * Returns the location of the resource resolved over the classpath
	 * 
	 * @param resourceLocation	the location of the resource on the classpath
	 * 
	 * @return	reference to an instance of {@see URL} which represents the location of the
	 * 			resource resolved over the classpath
	 */
	private URL getResourceUrl(String resourceLocation) {
		String sanitizedResourceLocation = sanitizeResourceLocation(resourceLocation);
		
		// attempt to find the resource using context class loader
		Optional<URL> optionalResourceUrl = getResourceUrl(sanitizedResourceLocation, 
				Thread.currentThread().getContextClassLoader());
		
		if (optionalResourceUrl.isPresent())
			return optionalResourceUrl.get();
			
		// if not found, attempt to find the resource using ClasspathResourceLoader's class loader
		optionalResourceUrl = getResourceUrl(sanitizedResourceLocation, ClasspathResourceLoader.class.getClassLoader());
		
		if (optionalResourceUrl.isPresent())
			return optionalResourceUrl.get();
		
		// if still not found, we cannot locate the resource
		throw new ResourceNotFoundException(resourceLocation, buildResourceNotFoundMessage(resourceLocation));
	}
	
	/**
	 * Returns a sanitized version of the specified resource location
	 * 
	 * @param resourceLocation	the resource location to be sanitized
	 * 
	 * @return	a sanitized version of the specified resource location
	 */
	private String sanitizeResourceLocation(String resourceLocation) {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(resourceLocation), 
				"A valid resource location must be specified");
		
		if (resourceLocation.charAt(0) == '/')
			return resourceLocation.substring(1);
		return resourceLocation;
	}

	/**
	 * Returns the location of the resource resolved over the classpath using the specified
	 * class loader
	 * 
	 * @param resourceLocation	the location of the resource on the classpath
	 * @param classLoader		reference to an instance of {@see ClassLoader} to use to resolve 
	 * 							the location of the resource
	 * 
	 * @return	reference to an instance of {@see Optional} which provides a {@see URL}
	 * 			that represent the location of the resource resolved over the classpath using
	 * 			the specified {@see ClassLoader} 
	 */
	private Optional<URL> getResourceUrl(String resourceLocation, ClassLoader classLoader) {
		return Optional.fromNullable(classLoader.getResource(resourceLocation));
	}

	/**
	 * Returns an error message for a resource not being found on the classpath at the specified 
	 * location
	 * 
	 * @param resourceLocation	the location of the resource on the classpath
	 * 
	 * @return	an error message for a resource not being found on the classpath at the specified 
	 * 			location
	 */
	private String buildResourceNotFoundMessage(String resourceLocation) {
		return String.format("Unable to find resource on classpath at the specified location: %s", 
				resourceLocation);
	}
}
