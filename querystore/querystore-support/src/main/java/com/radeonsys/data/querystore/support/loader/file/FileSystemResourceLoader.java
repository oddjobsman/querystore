/*
 * @(#)FileSystemResourceLoader.java				Thu May 31, 2012 11:52 AM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore.support.loader.file;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.io.Files;
import com.google.common.io.InputSupplier;

import com.radeonsys.data.querystore.support.loader.AbstractResourceLoader;
import com.radeonsys.data.querystore.support.loader.ResourceNotFoundException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 * Implements the {@see ResourceLoader} interface to provide loading resources off the
 * file-system.
 *
 * <p>This implementation supports loading both text resources as well as binary. Therefore
 * provides implementations of both the {@see ResourceLoader#getReadableResource} method
 * and the {@see ResourceLoader#getBinaryResource} method.</p>
 *
 * @author oddjobsman
 */
public class FileSystemResourceLoader extends AbstractResourceLoader {

    /**
     * Returns a factory that provides a {@see Reader} implementation over the resource at the
     * specified location on the file-system  using the specified character set to decode
     * the contents of the resource
     *
     * @param resourceLocation  the location of the resource that is required
     * @param charset           reference to an instance of {@see Charset} that represents the
     *                          character set used to decode the contents of the resource
     *
     * @return  reference to an instance of {@see InputSupplier} which provides instances of
     *          {@see Reader} over the resource at the specified location on the file-system
     */
    @Override
    public InputSupplier<? extends Reader> getReadableResource(String resourceLocation, Charset charset) {
        final File resourceFile = getResourceFile(resourceLocation);

        if (!isValidFileResource(resourceFile))
            throw new ResourceNotFoundException(resourceLocation, buildResourceNotFoundMessage(resourceLocation));

        return Files.newReaderSupplier(resourceFile, charset);
    }

    /**
     * Returns a factory that provides a {@see InputStream} implementation over the resource at the
     * specified location on the file-system
     *
     * @param resourceLocation  the location of the resource that is required
     *
     * @return  reference to an instance of {@see InputSupplier} which provides instances of
     *          {@see InputStream} over the resource at the specified location on the file-system
     */
    @Override
    public InputSupplier<? extends InputStream> getBinaryResource(String resourceLocation) {
        final File resourceFile = getResourceFile(resourceLocation);

        if (!isValidFileResource(resourceFile))
            throw new ResourceNotFoundException(resourceLocation, buildResourceNotFoundMessage(resourceLocation));

        return Files.newInputStreamSupplier(resourceFile);
    }

    /**
     * Returns a handle to the resource file
     *
     * <p>If the location of the resource file is a relative paths, it is resolved to an absolute path</p>
     *
     * @param resourceLocation  the location of the resource file
     *
     * @return  reference to an instance of {@see File} which represents a handle to the resource file
     */
    private File getResourceFile(String resourceLocation) {
    	Preconditions.checkArgument(!Strings.isNullOrEmpty(resourceLocation), 
    			"A valid resource locatin must be specified");
        
    	File file = new File(resourceLocation);
    	if (!file.isAbsolute()) {
	        try {
	                file = file.getCanonicalFile();
	        } catch (IOException e) {
	            throw new ResourceNotFoundException(resourceLocation, String.format(
	                    "Unable to resolve relative path to absolute path: %s", resourceLocation), e);
	        }
    	}

        return file;
    }

    /**
     * Returns whether or not the specified resource file handle points to a valid file
     *
     * @param resourceFile  reference to an instance of {@see File} which represents
     * @return
     */
    private boolean isValidFileResource(File resourceFile) {
        return resourceFile.exists() || resourceFile.isFile();
    }

    /**
     * Returns a resource not found error message for the specified resource location
     *
     * @param resourceLocation  the location of the resource that was not found
     *
     * @return a resource not found error message for the specified resource location
     */
    private String buildResourceNotFoundMessage(String resourceLocation) {
        return String.format("Unable to find file resource to be loaded: %s", resourceLocation);
    }
}
