/*
 * @(#)ResourceNotFoundException.java				Wed May 30, 2012 10:14 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore.support.loader;

/**
 * @author oddjobsman
 */
public class ResourceNotFoundException extends RuntimeException {

    /**
     * The generated serial version UID for this class
     */
    private static final long serialVersionUID = 7635156718268944430L;

    /**
     * Stores the location of the resource that could not be found
     */
    private final String resourceLocation;

    /**
     * Creates a new instance of {@code ResourceNotFoundException} with the
     * specified resource location
     *
     * @param resourceLocation  the location of the resource that could not be found
     */
    public ResourceNotFoundException(String resourceLocation) {
        super();
        this.resourceLocation = resourceLocation;
    }

    /**
     * Creates a new instance of {@code ResourceNotFoundException} with the
     * specified resource location and error message
     *
     * @param resourceLocation  the location of the resource that could not be found
     * @param message           the error message describing the error condition
     */
    public ResourceNotFoundException(String resourceLocation, String message) {
        super(message);
        this.resourceLocation = resourceLocation;
    }

    /**
     * Creates a new instance of {@code ResourceNotFoundException} with the
     * specified resource location and the underlying cause of error
     *
     * @param resourceLocation  the location of the resource that could not be found
     * @param cause             reference to an instance of {@see Throwable} which represents
     * 					        the underlying cause of the error
     */
    public ResourceNotFoundException(String resourceLocation, Throwable cause) {
        super(cause);
        this.resourceLocation = resourceLocation;
    }

    /**
     * Creates a new instance of {@code ResourceNotFoundException} with the specified
     * resource location, the specified error message and the underlying cause of error
     *
     * @param resourceLocation  the location of the resource that could not be found
     * @param message           the error message describing the error condition
     * @param cause             reference to an instance of {@see Throwable} which represents
     * 					        the underlying cause of the error
     */
    public ResourceNotFoundException(String resourceLocation, String message, Throwable cause) {
        super(message, cause);
        this.resourceLocation = resourceLocation;
    }

    /**
     * Returns the location of the resource that could not be found
     *
     * @return the location of the resource that could not be found
     */
    public String getResourceLocation() {
        return resourceLocation;
    }
}
