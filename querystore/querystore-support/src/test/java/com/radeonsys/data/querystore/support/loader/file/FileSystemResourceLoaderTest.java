/*
 * @(#)FileSystemResourceLoaderTests.java				Thu May 31, 2012 11:53 AM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore.support.loader.file;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import com.google.common.io.InputSupplier;
import com.radeonsys.data.querystore.support.loader.ResourceNotFoundException;

/**
 * Defines unit tests for {@see FileSystemResourceLoader}
 *
 * <p><b>NOTE:</b> These tests will actually create files in the local system during execution. Files could be created
 * at the following locations:</p>
 * <ol>
 *     <li>The startup directory from where your IDE/Tool (Maven, ANT) was started</li>
 *     <li>The project or workspace folder</li>
 *     <li>System designated temp dir</li>
 * </ol>
 * <p>
 * However, the tests will clean the affected folders up.
 * </p>
 *
 * @author oddjobsman
 */
public class FileSystemResourceLoaderTest {

    public static final long BINARY_TEST_DATA = 0xCAFEBABE;
    public static final String TEXT_TEST_DATA = "The quick brown fox jumped over the lazy dog.";

    private File tmpFile;
    private FileSystemResourceLoader loaderToTest;

    @Before
    public void setUp() throws IOException {
        loaderToTest = new FileSystemResourceLoader();
    }

    @Test
    public void getsReadableResourceFromFileSystem() throws Exception {
        try {
            tmpFile = createTextTempFile();
            InputSupplier<? extends Reader> streamSupplier = loaderToTest.getReadableResource(tmpFile.getPath());
            assertNotNull(streamSupplier);

            final String data = Files.readFirstLine(tmpFile, Charset.defaultCharset());
            assertEquals(TEXT_TEST_DATA, data);
        } finally {
            tmpFile.delete();
        }
    }

    @Test
    public void getsReadableResourceFromFileSystemWithCharset() throws Exception {
        try {
            tmpFile = createTextTempFile(Charsets.US_ASCII);
            InputSupplier<? extends Reader> streamSupplier = loaderToTest.getReadableResource(tmpFile.getPath());
            assertNotNull(streamSupplier);

            final String data = Files.readFirstLine(tmpFile, Charsets.US_ASCII);
            assertEquals(TEXT_TEST_DATA, data);
        } finally {
            tmpFile.delete();
        }
    }

    @Test
    public void getsReadableResourceFromFileSystemUsingRelativePath() throws Exception {
        try {
            tmpFile = createTextTempFile(createFileInParentDirOfAppDir());

            InputSupplier<? extends Reader> streamSupplier = loaderToTest.getReadableResource(tmpFile.getPath());
            assertNotNull(streamSupplier);

            final String data = Files.readFirstLine(tmpFile, Charset.defaultCharset());
            assertEquals(TEXT_TEST_DATA, data);
        } finally {
            tmpFile.delete();
        }
    }

    @Test
    public void getsReadableResourceFromFileSystemUsingRelativePathWithCharset() throws Exception {
        try {
            tmpFile = createTextTempFile(createFileInParentDirOfAppDir(), Charsets.US_ASCII);

            InputSupplier<? extends Reader> streamSupplier = loaderToTest.getReadableResource(tmpFile.getPath());
            assertNotNull(streamSupplier);

            final String data = Files.readFirstLine(tmpFile, Charsets.US_ASCII);
            assertEquals(TEXT_TEST_DATA, data);
        } finally {
            tmpFile.delete();
        }
    }

    @Test(expected=ResourceNotFoundException.class)
    public void throwsResourceNotFoundExceptionForMissingReadableFileResource() throws IOException {
        tmpFile = createEmptyTempFile();
        tmpFile.delete();
        loaderToTest.getReadableResource(tmpFile.getPath());
    }

    @Test
    public void getsBinaryResourceFromFileSystem() throws Exception {
        try {
            tmpFile = createBinaryTempFile();
            InputSupplier<? extends InputStream> streamSupplier = loaderToTest.getBinaryResource(tmpFile.getPath());
            assertNotNull(streamSupplier);

            ByteBuffer buffer = ByteBuffer.wrap(ByteStreams.toByteArray(streamSupplier));
            assertEquals(BINARY_TEST_DATA, buffer.getLong());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            tmpFile.delete();
        }
    }

    @Test
    public void getsBinaryResourceFromFileSystemUsingRelativePath() throws Exception {
        try {
            tmpFile = createBinaryTempFile(createFileInParentDirOfAppDir());

            InputSupplier<? extends InputStream> streamSupplier = loaderToTest.getBinaryResource("../tst.tmp");
            assertNotNull(streamSupplier);

            ByteBuffer buffer = ByteBuffer.wrap(ByteStreams.toByteArray(streamSupplier));
            assertEquals(BINARY_TEST_DATA, buffer.getLong());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            tmpFile.delete();
        }
    }

    @Test(expected=ResourceNotFoundException.class)
    public void throwsResourceNotFoundExceptionForMissingBinaryFileResource() throws IOException {
        tmpFile = createEmptyTempFile();
        tmpFile.delete();
        loaderToTest.getBinaryResource(tmpFile.getPath());
    }

    @After
    public void tearDown() {
        if (tmpFile != null)
            tmpFile.delete();
    }

    private File createFileInParentDirOfAppDir() {
        final File userDir = new File(System.getProperty("user.dir"));
        File userDirParent = userDir.getParentFile();
        return new File(userDirParent, "tst.tmp");
    }

    private File createBinaryTempFile() throws IOException {
        File file = File.createTempFile("qs_", "tst");
        return createBinaryTempFile(file);
    }

    private File createBinaryTempFile(File file) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(BINARY_TEST_DATA);

        Files.write(buffer.array(), file);
        return file;
    }

    private File createTextTempFile() throws IOException {
        File file = File.createTempFile("qs_", "tst");
        return createTextTempFile(file);
    }

    private File createTextTempFile(Charset charset) throws IOException {
        File file = File.createTempFile("qs_", "tst");
        return createTextTempFile(file, charset);
    }

    private File createTextTempFile(File file) throws IOException {
        return createTextTempFile(file, Charset.defaultCharset());
    }

    private File createTextTempFile(File file, Charset charset) throws IOException {
        Files.write(TEXT_TEST_DATA, file, charset);
        return file;
    }

    private File createEmptyTempFile() throws IOException {
        return File.createTempFile("qs_", "tst");
    }
}
