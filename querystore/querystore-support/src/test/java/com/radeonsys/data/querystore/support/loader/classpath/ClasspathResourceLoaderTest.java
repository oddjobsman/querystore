/*
 * @(#)ClasspathResourceLoader.java				Jun 1, 2012 11:31:30 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.radeonsys.data.querystore.support.loader.classpath;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

import org.junit.Test;

import com.google.common.io.InputSupplier;
import com.radeonsys.data.querystore.support.loader.ResourceNotFoundException;

/**
 * Defines unit tests for {@see ClasspathResourceLoader}
 * 
 * @author oddjobsman
 */
public class ClasspathResourceLoaderTest {
	
	@Test
	public void getsReadableResourceFromClasspath() throws IOException {
		InputSupplier<? extends Reader> readerSupplier = new ClasspathResourceLoader().getReadableResource(
						"com/radeonsys/data/querystore/support/loader/classpath/test.properties");
		
		assertNotNull(readerSupplier);
		
		Properties props = new Properties();
		props.load(readerSupplier.getInput());
		
		assertEquals("property", props.getProperty("test"));
	}
	
	@Test
	public void ignoresLeadingSlashBeforeGettingResourceFromClasspath() throws IOException {
		InputSupplier<? extends Reader> readerSupplier = new ClasspathResourceLoader().getReadableResource(
						"/com/radeonsys/data/querystore/support/loader/classpath/test.properties");
		
		assertNotNull(readerSupplier);
		
		Properties props = new Properties();
		props.load(readerSupplier.getInput());
		
		assertEquals("property", props.getProperty("test"));
	}
	
	@Test
    public void getsBinaryResourceFromFileSystem() throws Exception {
		InputSupplier<? extends InputStream> streamSupplier = new ClasspathResourceLoader().getBinaryResource(
				"com/radeonsys/data/querystore/support/loader/classpath/test.properties");
		
		assertNotNull(streamSupplier);
		
		Properties props = new Properties();
		props.load(streamSupplier.getInput());
		
		assertEquals("property", props.getProperty("test"));
    }
	
	@Test(expected=ResourceNotFoundException.class)
    public void throwsResourceNotFoundExceptionForMissingReadableClasspathResource() throws IOException {
		new ClasspathResourceLoader().getReadableResource("foo/bar/foo");
    }

}
