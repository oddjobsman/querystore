/*
 * @(#)AbstractResourceLoaderTest.java				Jun 2, 2012 9:10:17 AM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.radeonsys.data.querystore.support.loader;

import static org.junit.Assert.assertTrue;

import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.Maps;
import com.google.common.io.InputSupplier;

/**
 * Defines unit tests for {@see AbstractResourceLoader}
 * 
 * @author oddjobsman
 */
public class AbstractResourceLoaderTest {
	
	private static final String CALL_MADE_KEY = "callMade";

	@Test
	public void callToGetReadableResourceWithoutCharsetForwardsCallToGetReadableResourceWithCharset() {
		final Map<String, Boolean> state = Maps.newHashMap();
		state.put(CALL_MADE_KEY, false);
		
		AbstractResourceLoader testLoader = new AbstractResourceLoader() {
			@Override
			public InputSupplier<? extends Reader> getReadableResource(
					String resourceLocation, Charset charset) {
				if (charset.equals(Charset.defaultCharset()))
					state.put(CALL_MADE_KEY, true);
				return null;
			}
		};
		
		// make a call to the getReadableResource(String) method
		testLoader.getReadableResource("foo");
		
		assertTrue(state.get(CALL_MADE_KEY));
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void callToGetBinaryResourceThrowsUnsupportedOperationException() {
		
		AbstractResourceLoader testLoader = new AbstractResourceLoader() {
			@Override
			public InputSupplier<? extends Reader> getReadableResource(
					String resourceLocation, Charset charset) {
				// NOT RELEVANT
				return null;
			}
		};
		
		// call the getBinaryResource(String) method
		testLoader.getBinaryResource("foo");
	}

}
