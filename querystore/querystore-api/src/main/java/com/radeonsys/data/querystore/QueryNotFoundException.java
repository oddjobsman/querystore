/*
 * @(#)QueryNotFoundException.java				Mon May 28, 2012 10:27 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore;

import javax.annotation.Nonnull;

/**
 * Thrown to indicate that a query with a specified name was not
 * found in the query store
 *
 * @author oddjobsman
 */
public class QueryNotFoundException extends QueryStoreException {

    /**
     * The generated serial version UID for this class
     */
    private static final long serialVersionUID = -141396178462361266L;

    /**
     * Stores the name of the query that was not found
     */
    @Nonnull
    private final String _queryName;

    /**
     * Creates a new instance of {@code QueryNotFoundException} with the specified query name
     *
     * @param queryName     the name of the query that was not found
     */
    public QueryNotFoundException(@Nonnull final String queryName) {
        super();
        this._queryName = queryName;
    }

    /**
     * Creates a new instance of {@code QueryNotFoundException} with the specified query name and error message
     *
     * @param queryName     the name of the query that was not found
     * @param message       the error message describing the error condition
     */
    public QueryNotFoundException(@Nonnull final String queryName, final String message) {
        super(message);
        this._queryName = queryName;
    }

    /**
     * Creates a new instance of {@code QueryNotFoundException} with the specified query name and underlying cause
     * of the error
     *
     * @param queryName             the name of the query that was not found
     * @param underlyingCause       reference to an instance of {@see Throwable} which represents
     * 				                the underlying underlyingCause of the error
     */
    public QueryNotFoundException(@Nonnull final String queryName, final Throwable underlyingCause) {
        super(underlyingCause);
        this._queryName = queryName;
    }

    /**
     * Creates a new instance of {@code QueryNotFoundException} with the specified query name and underlying cause
     * of the error
     *
     * @param queryName             the name of the query that was not found
     * @param message               the error message describing the error condition
     * @param underlyingCause       reference to an instance of {@see Throwable} which represents
     * 				                the underlying underlyingCause of the error
     */
    public QueryNotFoundException(@Nonnull final String queryName,
                                  final String message,
                                  final Throwable underlyingCause) {
        super(message, underlyingCause);
        this._queryName = queryName;
    }

    /**
     * Returns the name of the query that was not found
     *
     * @return the name of the query that was not found
     */
    @Nonnull
    public String getQueryName() {
        return _queryName;
    }
}
