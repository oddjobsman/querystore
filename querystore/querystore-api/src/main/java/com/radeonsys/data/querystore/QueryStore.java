/*
 * @(#)QueryStore.java				Fri May 25, 2012 08:33 AM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore;

import javax.annotation.Nonnull;

/**
 * Represents a store of query string that can be looked up by name
 *
 * @author oddjobsman
 */
public interface QueryStore {

    /**
     * Returns the query string from this query store with the specified
     * name
     *
     * @param nameOfQuery   name of the query that is required
     *
     * @return the query string from this query store with the specified name
     *
     * @throws QueryNotFoundException
     *          if no query string with the specified name was found in this {@code QueryStore}
     * @throws QueryLoadException
     *          if there was an error loading the query with the specified name
     */
    String getQuery(@Nonnull String nameOfQuery);

}
