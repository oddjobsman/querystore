/*
 * @(#)QueryStoreException.java				Mon May 28, 2012 10:24 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore;

/**
 * Indicates an error that occurred in the query store during
 * an operation
 *
 * @author oddjobsman
 */
public class QueryStoreException extends RuntimeException {

    /**
     * The generated serial version UID for this class
     */
    private static final long serialVersionUID = 3425715257912516787L;

    /**
     * Creates a new instance of {@code QueryStoreException}
     */
    public QueryStoreException() {
        // DO NOTHING
    }

    /**
     * Creates a new instance of {@code QueryStoreException} with the
     * specified error message
     *
     * @param message	the error message describing the error condition
     */
    public QueryStoreException(final String message) {
        super(message);
    }

    /**
     * Creates a new instance of {@code QueryStoreException} with the
     * specified underlying cause of the error
     *
     * @param underlyingCause	    reference to an instance of {@see Throwable} which represents
     * 				                the underlying cause of the error
     */
    public QueryStoreException(final Throwable underlyingCause) {
        super(underlyingCause);
    }

    /**
     * Creates a new instance of {@code QueryStoreException} with the
     * specified error message and underlying cause of the error
     *
     * @param message	            the error message describing the error condition
     * @param underlyingCause		reference to an instance of {@see Throwable} which represents
     * 					            the underlying cause of the error
     */
    public QueryStoreException(final String message, final Throwable underlyingCause) {
        super(message, underlyingCause);
    }

}
