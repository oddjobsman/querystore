/*
 * @(#)QueryLoadException.java				Wed May 30, 2012 04:33 PM
 *
 * Copyright 2012 Radeon Systems
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.radeonsys.data.querystore;

/**
 * Thrown when a {@see QueryStore} is unable to load a query
 *
 * @author oddjobsman
 */
public class QueryLoadException extends QueryStoreException {

    /**
     * The generated serial version UID for this class
     */
    private static final long serialVersionUID = -6489533485586295558L;

    /**
     * Creates a new instance of {@code QueryLoadException}
     */
    public QueryLoadException() {
        // DO NOTHING
    }

    /**
     * Creates a new instance of {@code QueryLoadException} with the specified error
     * message
     *
     * @param message               the error message describing the error condition
     */
    public QueryLoadException(String message) {
        super(message);
    }

    /**
     * Creates a new instance of {@code QueryLoadException} with
     * the specified underlying cause
     *
     * @param underlyingCause		reference to an instance of {@see Throwable} which represents
     * 					            the underlying cause of the error
     */
    public QueryLoadException(Throwable underlyingCause) {
        super(underlyingCause);
    }

    /**
     * Creates a new instance of {@code QueryLoadException} with
     * the specified error message and underlying cause
     *
     * @param message               the error message describing the error condition
     * @param underlyingCause		reference to an instance of {@see Throwable} which represents
     * 					            the underlying cause of the error
     */
    public QueryLoadException(String message, Throwable underlyingCause) {
        super(message, underlyingCause);
    }

}
